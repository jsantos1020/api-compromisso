package br.com.jcsistemas.api.compromisso.util;

import java.io.Serializable;

public class ConstantesApi implements Serializable {

	public static String TIPO_NAO_ENCONTRADO = "Não foi possível encontrar o tipo de compromisso na base de dados!";
	public static String COMPROMISSO_NAO_ENCONTRADO = "Não foi possível encontrar o compromisso na base de dados!";
}
