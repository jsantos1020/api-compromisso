package br.com.jcsistemas.api.compromisso.repository;

import br.com.jcsistemas.api.compromisso.entity.TipoCompromisso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoCompromissoRepository extends JpaRepository<TipoCompromisso, Integer> {
}