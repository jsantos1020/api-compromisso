package br.com.jcsistemas.api.compromisso.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TipoCompromissoDTO implements Serializable {

	private Integer codigo;
	@JsonProperty
	private String descricao;
}