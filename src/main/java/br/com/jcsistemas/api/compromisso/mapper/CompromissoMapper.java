package br.com.jcsistemas.api.compromisso.mapper;

import br.com.jcsistemas.api.compromisso.dto.CompromissoDTO;
import br.com.jcsistemas.api.compromisso.entity.Compromisso;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = { TipoCompromissoMapper.class })
public interface CompromissoMapper {

	CompromissoDTO toDto(Compromisso compromisso);
	Compromisso toEntity(CompromissoDTO compromissoDTO);
	List<CompromissoDTO> toEntity(List<Compromisso> compromissos);
}