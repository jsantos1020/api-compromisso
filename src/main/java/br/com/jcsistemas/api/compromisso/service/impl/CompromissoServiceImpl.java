package br.com.jcsistemas.api.compromisso.service.impl;

import br.com.jcsistemas.api.compromisso.dto.CompromissoDTO;
import br.com.jcsistemas.api.compromisso.entity.Compromisso;
import br.com.jcsistemas.api.compromisso.exception.CodigoNaoEncontradoException;
import br.com.jcsistemas.api.compromisso.mapper.CompromissoMapper;
import br.com.jcsistemas.api.compromisso.repository.CompromissoRepository;
import br.com.jcsistemas.api.compromisso.service.api.CompromissoService;
import br.com.jcsistemas.api.compromisso.util.ConstantesApi;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompromissoServiceImpl implements CompromissoService {

	@Autowired
	private CompromissoRepository repository;
	@Autowired
	private CompromissoMapper mapper;

	@Override
	public CompromissoDTO salvar(CompromissoDTO compromissoDTO) {
		return mapper.toDto(repository.save(mapper.toEntity(compromissoDTO)));
	}

	@Override
	public CompromissoDTO buscarPorId(Integer codigo) {
		return mapper.toDto(repository.findById(codigo)
									  .orElseThrow(() -> new CodigoNaoEncontradoException(ConstantesApi.COMPROMISSO_NAO_ENCONTRADO)));
	}

	@Override
	public CompromissoDTO atualizarPorId(Integer codigo, CompromissoDTO compromissoDTO) {
		Compromisso compromissoAtualiza = mapper.toEntity(buscarPorId(codigo));
		BeanUtils.copyProperties(mapper.toEntity(compromissoDTO), compromissoAtualiza, "codigo");
		return mapper.toDto(repository.save(compromissoAtualiza));
	}

	@Override
	public void apagaPorId(Integer codigo) {
		repository.deleteById(codigo);
	}

	@Override
	public List<CompromissoDTO> buscar() {
		return mapper.toEntity(repository.findAll());
	}
}