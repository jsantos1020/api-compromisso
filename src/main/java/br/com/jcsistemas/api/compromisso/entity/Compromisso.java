package br.com.jcsistemas.api.compromisso.entity;

import br.com.jcsistemas.api.compromisso.enumeration.ClassificacaoCompromisso;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "COMPROMISSO")
public class Compromisso implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IDCOMPROMISSO")
	private Integer codigo;
	@Column(name = "COMPROMISSO")
	private String descricao;
	@Column(name = "DATAINICIO")
	private LocalDateTime dataInicio;
	@Column(name = "DATATERMINO")
	private LocalDateTime dataTermino;
	@Column(name = "TITULO")
	private String titulo;
	@Column(name = "CLASSIFICACAO_COMPROMISSO", columnDefinition = "VARCHAR(20)")
	@Enumerated(EnumType.STRING)
	private ClassificacaoCompromisso classificacaoCompromisso;
	@ManyToOne
	@JoinColumn(name = "IDTIPO")
	private TipoCompromisso tipoCompromisso;
}