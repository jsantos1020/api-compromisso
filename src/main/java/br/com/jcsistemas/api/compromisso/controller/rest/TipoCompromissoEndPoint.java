package br.com.jcsistemas.api.compromisso.controller.rest;

import br.com.jcsistemas.api.compromisso.dto.TipoCompromissoDTO;
import br.com.jcsistemas.api.compromisso.service.api.TipoCompromissoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class TipoCompromissoEndPoint {

	public static final String URI_PATH_TIPOS_COMPROMISSOS = "/tipos-compromissos";
	public static final String URI_PATH_TIPOS_COMPROMISSOS_ID = URI_PATH_TIPOS_COMPROMISSOS + "/{codigo}";

	@Autowired
	private TipoCompromissoService service;

	@PostMapping(URI_PATH_TIPOS_COMPROMISSOS)
	public ResponseEntity<TipoCompromissoDTO> salvar(@RequestBody TipoCompromissoDTO tipoCompromissoDTO) {
		TipoCompromissoDTO tipoCompromissoRespostaDTO = service.salvar(tipoCompromissoDTO);
		return ResponseEntity.status(HttpStatus.CREATED).body(tipoCompromissoRespostaDTO);
	}

	@GetMapping(URI_PATH_TIPOS_COMPROMISSOS)
	public List<TipoCompromissoDTO> buscarTodos() {
		return service.buscar();
	}

	@GetMapping(URI_PATH_TIPOS_COMPROMISSOS_ID)
	public ResponseEntity<TipoCompromissoDTO> buscarPeloCodigo(@PathVariable Integer codigo) {
		return ResponseEntity.ok(service.buscarPorId(codigo));
	}

	@PutMapping(URI_PATH_TIPOS_COMPROMISSOS_ID)
	public ResponseEntity<TipoCompromissoDTO> atualiza(@PathVariable Integer codigo, @RequestBody TipoCompromissoDTO tipoCompromissoDTO) {
		return ResponseEntity.ok(service.atualizarPorId(codigo, tipoCompromissoDTO));
	}

	@DeleteMapping(URI_PATH_TIPOS_COMPROMISSOS_ID)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public List<TipoCompromissoDTO> remover(@PathVariable Integer codigo) {
		service.apagaPorId(codigo);
		return service.buscar();
	}
}