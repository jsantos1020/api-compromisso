package br.com.jcsistemas.api.compromisso.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ClassificacaoCompromisso {

	IMPORTANTE(1, "Importante"),
	URGENTE(2, "Urgente"),
	NORMAL(3, "Normal");

	private Integer codigo;
	private String descricao;
}