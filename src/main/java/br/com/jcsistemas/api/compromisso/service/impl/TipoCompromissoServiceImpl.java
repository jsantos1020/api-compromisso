package br.com.jcsistemas.api.compromisso.service.impl;

import br.com.jcsistemas.api.compromisso.util.ConstantesApi;
import br.com.jcsistemas.api.compromisso.dto.TipoCompromissoDTO;
import br.com.jcsistemas.api.compromisso.entity.TipoCompromisso;
import br.com.jcsistemas.api.compromisso.exception.CodigoNaoEncontradoException;
import br.com.jcsistemas.api.compromisso.mapper.TipoCompromissoMapper;
import br.com.jcsistemas.api.compromisso.repository.TipoCompromissoRepository;
import br.com.jcsistemas.api.compromisso.service.api.TipoCompromissoService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TipoCompromissoServiceImpl implements TipoCompromissoService {

	@Autowired
	private TipoCompromissoRepository repository;

	@Autowired
	private TipoCompromissoMapper mapper;

	@Override
	public TipoCompromissoDTO salvar(TipoCompromissoDTO tipoCompromissoDTO) {
		return mapper.tipoCompromissoDTO(repository.save(mapper.tipoCompromisso(tipoCompromissoDTO)));
	}

	@Override
	public TipoCompromissoDTO buscarPorId(Integer codigo) {
		return mapper.tipoCompromissoDTO(repository.findById(codigo)
			                                       .orElseThrow(() -> new CodigoNaoEncontradoException(ConstantesApi.TIPO_NAO_ENCONTRADO)));
	}

	@Override
	public TipoCompromissoDTO atualizarPorId(Integer codigo, TipoCompromissoDTO tipoCompromissoDTO) {
		TipoCompromisso tipoCompromissoAtualiza = mapper.tipoCompromisso(buscarPorId(codigo));
		BeanUtils.copyProperties(mapper.tipoCompromisso(tipoCompromissoDTO), tipoCompromissoAtualiza, "codigo");
		return mapper.tipoCompromissoDTO(repository.save(tipoCompromissoAtualiza));
	}

	@Override
	public void apagaPorId(Integer codigo) {
		repository.deleteById(codigo);
	}

	@Override
	public List<TipoCompromissoDTO> buscar() {
		return mapper.tiposDeCompromisso(repository.findAll());
	}
}