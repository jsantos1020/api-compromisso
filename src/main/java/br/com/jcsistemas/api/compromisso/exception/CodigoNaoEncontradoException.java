package br.com.jcsistemas.api.compromisso.exception;

public class CodigoNaoEncontradoException extends RuntimeException {

	private static final long serialVersionUID = 1149241039409861914L;

	public CodigoNaoEncontradoException() {
		super();
	}

	public CodigoNaoEncontradoException(String message) {
		super(message);
	}

	public CodigoNaoEncontradoException(String message, Throwable throwable) {
		super(message, throwable);
	}
}