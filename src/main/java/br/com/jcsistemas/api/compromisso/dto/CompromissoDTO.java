package br.com.jcsistemas.api.compromisso.dto;

import br.com.jcsistemas.api.compromisso.enumeration.ClassificacaoCompromisso;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CompromissoDTO implements Serializable {

	private Integer codigo;
	private String descricao;
	private LocalDateTime dataInicio;
	private LocalDateTime dataTermino;
	private String titulo;
	private TipoCompromissoDTO tipoCompromissoDTO;
	private ClassificacaoCompromisso classificacaoCompromisso;
}
