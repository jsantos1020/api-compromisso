package br.com.jcsistemas.api.compromisso.service.api;

import br.com.jcsistemas.api.compromisso.dto.TipoCompromissoDTO;

import java.util.List;

public interface TipoCompromissoService {

	TipoCompromissoDTO salvar(TipoCompromissoDTO tipoCompromissoDTO);
	TipoCompromissoDTO buscarPorId(Integer codigo);
	TipoCompromissoDTO atualizarPorId(Integer codigo, TipoCompromissoDTO tipoCompromissoDTO);
	void apagaPorId(Integer codigo);
	List<TipoCompromissoDTO> buscar();
}