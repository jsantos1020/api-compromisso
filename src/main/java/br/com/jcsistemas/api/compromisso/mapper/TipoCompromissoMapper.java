package br.com.jcsistemas.api.compromisso.mapper;

import br.com.jcsistemas.api.compromisso.dto.TipoCompromissoDTO;
import br.com.jcsistemas.api.compromisso.entity.TipoCompromisso;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract interface TipoCompromissoMapper {

	TipoCompromissoDTO tipoCompromissoDTO (TipoCompromisso tipoCompromisso);
	TipoCompromisso tipoCompromisso (TipoCompromissoDTO tipoCompromissoDTO);
	List<TipoCompromissoDTO> tiposDeCompromisso(List<TipoCompromisso> tipoDeCompromissos);
}