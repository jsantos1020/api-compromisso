package br.com.jcsistemas.api.compromisso.service.api;

import br.com.jcsistemas.api.compromisso.dto.CompromissoDTO;

import java.util.List;

public interface CompromissoService {

	CompromissoDTO salvar(CompromissoDTO compromissoDTO);
	CompromissoDTO buscarPorId(Integer codigo);
	CompromissoDTO atualizarPorId(Integer codigo, CompromissoDTO compromissoDTO);
	void apagaPorId(Integer codigo);
	List<CompromissoDTO> buscar();
}