package br.com.jcsistemas.api.compromisso.repository;

import br.com.jcsistemas.api.compromisso.entity.Compromisso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompromissoRepository extends JpaRepository<Compromisso, Integer> {
}