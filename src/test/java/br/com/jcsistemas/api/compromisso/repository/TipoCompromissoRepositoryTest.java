package br.com.jcsistemas.api.compromisso.repository;

import br.com.jcsistemas.api.compromisso.entity.TipoCompromisso;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

@DataJpaTest
public class TipoCompromissoRepositoryTest {

	@Autowired
	private TipoCompromissoRepository repository;

	@Test
	@DisplayName("Salvar o tipo do compromisso")
	public void salvarTipoCompromisso() {
		TipoCompromisso resultado = repository.save(criaEntity());

		Assertions.assertThat(resultado.getCodigo()).isNotNull();
	}

	private TipoCompromisso criaEntity() {
		return TipoCompromisso.builder().descricao("Fazer a lição com o Isaque").build();
	}
}