package br.com.jcsistemas.api.compromisso.controller.rest;

import br.com.jcsistemas.api.compromisso.dto.TipoCompromissoDTO;
import br.com.jcsistemas.api.compromisso.exception.CodigoNaoEncontradoException;
import br.com.jcsistemas.api.compromisso.service.api.TipoCompromissoService;
import br.com.jcsistemas.api.compromisso.util.ConstantesApi;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TipoCompromissoEndPointTest {

	public static final String URI_PATH_TIPOS_COMPROMISSOS = "/api/tipos-compromissos";
	public static final String URI_PATH_TIPOS_COMPROMISSOS_ID = URI_PATH_TIPOS_COMPROMISSOS + "/{codigo}";
	@MockBean
	private TipoCompromissoService service;
	@Autowired
	private TestRestTemplate restTemplate;
	private TipoCompromissoDTO tipoCompromissoDto;
	private static Integer CODIGO = 1;
	private static String DESCRICAO = "Reuniões";
	private static Integer CODIGO_INEXISTENTE = 99;

	@BeforeEach
	public void setUp() {
		criaDTO();
	}

	@Test
	@DisplayName("Retorna o status 201, quando salva o Tipo Compromisso")
	public void salvarTipoCompromissoRetornarCodigo201() {
		when(service.salvar(any(TipoCompromissoDTO.class))).thenReturn(tipoCompromissoDto);
		ResponseEntity<TipoCompromissoDTO> response = restTemplate.postForEntity(URI_PATH_TIPOS_COMPROMISSOS, tipoCompromissoDto, TipoCompromissoDTO.class);

		assertThat(response.getStatusCodeValue()).isEqualTo(201);
	}

	@Test
	@DisplayName("Retorna o status 200, quando buscar todos os tipo de compromisso")
	public void buscarTodosTipoCompromissoRetornarCodigo200() {
		when(service.buscar()).thenReturn(montaListaDto());
		ResponseEntity<List<TipoCompromissoDTO>> response = restTemplate.exchange(URI_PATH_TIPOS_COMPROMISSOS,
			HttpMethod.GET,
			preparaRequestEntity(),
			new ParameterizedTypeReference<List<TipoCompromissoDTO>>() {
			});
		assertThat(response.getStatusCodeValue()).isEqualTo(200);
	}

	@Test
	@DisplayName("Retorna o status 200, quando buscar pelo código")
	public void buscarPeloCodigoRetornarCodigo200() {
		when(service.buscarPorId(any(Integer.class))).thenReturn(tipoCompromissoDto);
		ResponseEntity<TipoCompromissoDTO> response = restTemplate.getForEntity(URI_PATH_TIPOS_COMPROMISSOS_ID, TipoCompromissoDTO.class, CODIGO);

		assertThat(response.getStatusCodeValue()).isEqualTo(200);
	}

	@Test
	@DisplayName("Retorna o status 500, quando buscar pelo código")
	public void buscarPeloCodigoRetornarCodigo500() {
		when(service.buscarPorId(CODIGO_INEXISTENTE)).thenThrow(new CodigoNaoEncontradoException(ConstantesApi.TIPO_NAO_ENCONTRADO));
		ResponseEntity<TipoCompromissoDTO> response = restTemplate.getForEntity(URI_PATH_TIPOS_COMPROMISSOS_ID, TipoCompromissoDTO.class, CODIGO_INEXISTENTE);

		assertThat(response.getStatusCodeValue()).isEqualTo(500);
	}

	private void criaDTO() {
		tipoCompromissoDto = TipoCompromissoDTO.builder().codigo(CODIGO).descricao(DESCRICAO).build();
	}

	private List<TipoCompromissoDTO> montaListaDto() {
		return Arrays.asList(new TipoCompromissoDTO(2, "Pessoais"),
			new TipoCompromissoDTO(3, "Trabalho"));
	}

	private HttpEntity<TipoCompromissoDTO> preparaRequestEntity() {
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		return new HttpEntity<>(requestHeaders);
	}
}