package br.com.jcsistemas.api.compromisso.service;

import br.com.jcsistemas.api.compromisso.dto.CompromissoDTO;
import br.com.jcsistemas.api.compromisso.dto.TipoCompromissoDTO;
import br.com.jcsistemas.api.compromisso.entity.Compromisso;
import br.com.jcsistemas.api.compromisso.entity.TipoCompromisso;
import br.com.jcsistemas.api.compromisso.enumeration.ClassificacaoCompromisso;
import br.com.jcsistemas.api.compromisso.exception.CodigoNaoEncontradoException;
import br.com.jcsistemas.api.compromisso.mapper.CompromissoMapper;
import br.com.jcsistemas.api.compromisso.repository.CompromissoRepository;
import br.com.jcsistemas.api.compromisso.service.api.CompromissoService;
import br.com.jcsistemas.api.compromisso.util.ConstantesApi;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
public class CompromissoServiceImplTest {

	public static final int CODIGO = 1;
	public static final int CODIGO_INEXISTENTE = 99;
	@Autowired
	private CompromissoService service;
	@MockBean
	private CompromissoRepository repository;
	@MockBean
	private CompromissoMapper mapper;

	private CompromissoDTO compromissoDTO;
	private CompromissoDTO compromissoDTOResultado;
	private Compromisso compromisso;
	private Compromisso compromissoResultado;

	@BeforeEach
	public void setup() {
		criaDto();
		criaEntity();
	}

	@Test
	@DisplayName("Salvar o compromisso")
	public void salvarCompromisso() {
		preparaMockSalvar();
		CompromissoDTO resultado = service.salvar(compromissoDTO);

		assertThat(resultado.getCodigo()).isNotNull();
		assertThat(resultado.getTitulo()).isEqualTo(compromissoDTOResultado.getTitulo());
		assertThat(resultado.getDescricao()).isEqualTo(compromissoDTOResultado.getDescricao());
		assertThat(resultado.getClassificacaoCompromisso().getDescricao()).isEqualTo(compromissoDTOResultado.getClassificacaoCompromisso().getDescricao());
		assertThat(resultado.getDataInicio()).isEqualTo(compromissoDTOResultado.getDataInicio());
		assertThat(resultado.getDataTermino()).isEqualTo(compromissoDTOResultado.getDataTermino());
		assertThat(resultado.getTipoCompromissoDTO().getCodigo()).isNotNull();
	}

	@Test
	@DisplayName("Buscar todos os compromissos")
	public void buscarTodosCompromissos() {
		preparaMockBuscarTodos();
		List<CompromissoDTO> dtos = service.buscar();

		verify(repository).findAll();
		assertThat(dtos).hasSize(2);
	}

	@Test
	@DisplayName("Buscar pelo código do compromisso")
	public void buscarPeloCodigo() {
		preparaMockBuscarCodigo();
		CompromissoDTO resultado = service.buscarPorId(CODIGO);

		assertThat(resultado.getCodigo()).isNotNull();
		assertThat(resultado.getTitulo()).isEqualTo(compromissoDTOResultado.getTitulo());
		assertThat(resultado.getDescricao()).isEqualTo(compromissoDTOResultado.getDescricao());
		assertThat(resultado.getClassificacaoCompromisso().getDescricao()).isEqualTo(compromissoDTOResultado.getClassificacaoCompromisso().getDescricao());
		assertThat(resultado.getDataInicio()).isEqualTo(compromissoDTOResultado.getDataInicio());
		assertThat(resultado.getDataTermino()).isEqualTo(compromissoDTOResultado.getDataTermino());
		assertThat(resultado.getTipoCompromissoDTO().getCodigo()).isNotNull();
	}

	@Test
	@DisplayName("Atualiza o compromisso")
	public void atualizarCompromisso() {
		preparaMockAtualiza();

		CompromissoDTO resultado = service.atualizarPorId(CODIGO, compromissoDTO);

		assertThat(resultado.getCodigo()).isNotNull();
		assertThat(resultado.getTitulo()).isEqualTo(compromissoDTOResultado.getTitulo());
		assertThat(resultado.getDescricao()).isEqualTo(compromissoDTOResultado.getDescricao());
		assertThat(resultado.getClassificacaoCompromisso().getDescricao()).isEqualTo(compromissoDTOResultado.getClassificacaoCompromisso().getDescricao());
		assertThat(resultado.getDataInicio()).isEqualTo(compromissoDTOResultado.getDataInicio());
		assertThat(resultado.getDataTermino()).isEqualTo(compromissoDTOResultado.getDataTermino());
		assertThat(resultado.getTipoCompromissoDTO().getCodigo()).isNotNull();
		verify(repository).save(mapper.toEntity(compromissoDTO));

	}

	@Test
	@DisplayName("Não encontra o compromisso pelo código")
	public void compromissoNaoEncontrado() {
		CodigoNaoEncontradoException exception = Assertions.assertThrows(CodigoNaoEncontradoException.class, () -> {
			service.buscarPorId(CODIGO_INEXISTENTE);
		});
		assertThat(exception.getMessage()).isEqualTo(ConstantesApi.COMPROMISSO_NAO_ENCONTRADO);
	}

	@Test
	@DisplayName("Apaga o compromisso pelo código")
	public void apagarCompromisso() {
		service.apagaPorId(CODIGO);
		verify(repository).deleteById(CODIGO);
	}

	private void criaEntity() {
		// TODO: Melhorar esse método criaEntity
		compromisso = Compromisso.builder()
			.descricao("Pagar a Cantina do Isaque")
			.titulo("Pagar a Márcia")
			.classificacaoCompromisso(ClassificacaoCompromisso.IMPORTANTE)
			.dataInicio(LocalDateTime.now())
			.dataTermino(LocalDateTime.now())
			.tipoCompromisso(TipoCompromisso.builder().codigo(1).descricao("Escola").build())
			.build();

		compromissoResultado = Compromisso.builder()
			.codigo(1)
			.descricao("Pagar a Cantina do Isaque")
			.titulo("Pagar a Márcia")
			.classificacaoCompromisso(ClassificacaoCompromisso.IMPORTANTE)
			.dataInicio(LocalDateTime.now())
			.dataTermino(LocalDateTime.now())
			.tipoCompromisso(TipoCompromisso.builder().codigo(1).descricao("Escola").build())
			.build();
	}

	private void criaDto() {
		// TODO: Melhorar esse método criaDto
		compromissoDTO = CompromissoDTO.builder()
			.descricao("Pagar a Cantina do Isaque")
			.titulo("Pagar a Márcia")
			.classificacaoCompromisso(ClassificacaoCompromisso.IMPORTANTE)
			.dataInicio(LocalDateTime.now())
			.dataTermino(LocalDateTime.now())
			.tipoCompromissoDTO(TipoCompromissoDTO.builder().codigo(1).descricao("Escola").build())
			.build();

		compromissoDTOResultado = CompromissoDTO.builder()
			.codigo(1)
			.descricao("Pagar a Cantina do Isaque")
			.titulo("Pagar a Márcia")
			.classificacaoCompromisso(ClassificacaoCompromisso.IMPORTANTE)
			.dataInicio(LocalDateTime.now())
			.dataTermino(LocalDateTime.now())
			.tipoCompromissoDTO(TipoCompromissoDTO.builder().codigo(1).descricao("Escola").build())
			.build();
	}

	private List<Compromisso> montarLista() {
		// TODO: Melhorar esse método montarLista
		return Arrays.asList(Compromisso.builder()
				.descricao("Pagar a Cantina do Isaque")
				.titulo("Pagar a Márcia")
				.classificacaoCompromisso(ClassificacaoCompromisso.IMPORTANTE)
				.dataInicio(LocalDateTime.now())
				.dataTermino(LocalDateTime.now())
				.tipoCompromisso(TipoCompromisso.builder().codigo(1).descricao("Escola").build())
				.build(),
			Compromisso.builder()
				.descricao("Comprar frutas")
				.titulo("Levar frutas para Escola")
				.classificacaoCompromisso(ClassificacaoCompromisso.IMPORTANTE)
				.dataInicio(LocalDateTime.now())
				.dataTermino(LocalDateTime.now())
				.tipoCompromisso(TipoCompromisso.builder().codigo(1).descricao("Escola").build())
				.build());
	}

	private List<CompromissoDTO> montarListaDTO() {
		// TODO: Melhorar esse método montarListaDTO
		return Arrays.asList(CompromissoDTO.builder()
				.descricao("Pagar a Cantina do Isaque")
				.titulo("Pagar a Márcia")
				.classificacaoCompromisso(ClassificacaoCompromisso.IMPORTANTE)
				.dataInicio(LocalDateTime.now())
				.dataTermino(LocalDateTime.now())
				.tipoCompromissoDTO(TipoCompromissoDTO.builder().codigo(1).descricao("Escola").build())
				.build(),
			CompromissoDTO.builder()
				.descricao("Comprar frutas")
				.titulo("Levar frutas para Escola")
				.classificacaoCompromisso(ClassificacaoCompromisso.IMPORTANTE)
				.dataInicio(LocalDateTime.now())
				.dataTermino(LocalDateTime.now())
				.tipoCompromissoDTO(TipoCompromissoDTO.builder().codigo(1).descricao("Escola").build())
				.build());
	}

	private void preparaMockBuscarTodos() {
		when(repository.findAll()).thenReturn(montarLista());
		when(mapper.toEntity(anyList())).thenReturn(montarListaDTO());
	}

	private void preparaMockAtualiza() {
		when(repository.findById(anyInt())).thenReturn(Optional.of(compromissoResultado));
		when(mapper.toEntity(any(CompromissoDTO.class))).thenReturn(compromissoResultado);
		when(repository.save(any(Compromisso.class))).thenReturn(compromissoResultado);
		when(mapper.toDto(any(Compromisso.class))).thenReturn(compromissoDTOResultado);
	}

	private void preparaMockSalvar() {
		when(mapper.toEntity(any(CompromissoDTO.class))).thenReturn(compromissoResultado);
		when(repository.save(any(Compromisso.class))).thenReturn(compromissoResultado);
		when(mapper.toDto(any(Compromisso.class))).thenReturn(compromissoDTOResultado);
	}

	private void preparaMockException() {
		when(repository.findById(anyInt()).orElseThrow(() -> new CodigoNaoEncontradoException(ConstantesApi.COMPROMISSO_NAO_ENCONTRADO)));
	}

	private void preparaMockBuscarCodigo() {
		when(repository.findById(any(Integer.class))).thenReturn(Optional.of(compromissoResultado));
		when(mapper.toDto(any(Compromisso.class))).thenReturn(compromissoDTOResultado);
	}
}
