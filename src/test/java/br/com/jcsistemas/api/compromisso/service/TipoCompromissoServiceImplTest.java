package br.com.jcsistemas.api.compromisso.service;

import br.com.jcsistemas.api.compromisso.dto.TipoCompromissoDTO;
import br.com.jcsistemas.api.compromisso.entity.TipoCompromisso;
import br.com.jcsistemas.api.compromisso.exception.CodigoNaoEncontradoException;
import br.com.jcsistemas.api.compromisso.mapper.TipoCompromissoMapper;
import br.com.jcsistemas.api.compromisso.repository.TipoCompromissoRepository;
import br.com.jcsistemas.api.compromisso.service.api.TipoCompromissoService;
import br.com.jcsistemas.api.compromisso.util.ConstantesApi;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
public class TipoCompromissoServiceImplTest {

	@Autowired
	private TipoCompromissoService service;
	@MockBean
	private TipoCompromissoRepository repository;
	@MockBean
	private TipoCompromissoMapper mapper;

	private TipoCompromissoDTO tipoCompromissoDto;
	private TipoCompromisso tipoCompromisso;

	private static Integer CODIGO = 1;
	private static String DESCRICAO = "Reuniões";
	private static Integer CODIGO_INEXISTENTE = 99;

	@BeforeEach
	public void setUp() {
		criaDTO();
		criaTipoCompromisso();
	}

	@Test
	@DisplayName("Salvar o tipo compromisso")
	public void salvarTipoCompromisso() {
		preparaMockSalvar();
		TipoCompromissoDTO resultado = service.salvar(tipoCompromissoDto);

		assertThat(resultado.getCodigo()).isNotNull();
		assertThat(resultado.getCodigo()).isEqualTo(tipoCompromissoDto.getCodigo());
		assertThat(resultado.getDescricao()).isEqualTo(tipoCompromissoDto.getDescricao());
		verify(repository).save(mapper.tipoCompromisso(tipoCompromissoDto));
	}

	@Test
	@DisplayName("Atualiza o tipo compromisso")
	public void atualizarTipoCompromisso() {
		preparaMockAtualizar();
		TipoCompromissoDTO resultado = service.atualizarPorId(CODIGO, tipoCompromissoDto);

		assertThat(resultado.getCodigo()).isEqualTo(tipoCompromissoDto.getCodigo());
		assertThat(resultado.getDescricao()).isEqualTo(tipoCompromissoDto.getDescricao());
		verify(repository).save(mapper.tipoCompromisso(tipoCompromissoDto));
	}

	@Test
	@DisplayName("Busca todos os tipos de compromissos")
	public void buscarTodosTiposCompromissos() {
		preparaMockBuscarDados();

		List<TipoCompromissoDTO> dtos = service.buscar();
		assertThat(dtos).hasSize(2);
	}

	@Test
	@DisplayName("Busca pelo código do tipo de compromisso")
	public void buscarPeloCodigo() {
		preparaMockBuscarPeloCodigo();

		TipoCompromissoDTO resultado = service.buscarPorId(CODIGO);
		assertThat(resultado.getCodigo()).isNotNull();
		assertThat(resultado.getCodigo()).isEqualTo(tipoCompromissoDto.getCodigo());
		assertThat(resultado.getDescricao()).isEqualTo(tipoCompromissoDto.getDescricao());
		verify(repository).findById(CODIGO);
	}

	@Test
	@DisplayName("Não encontra o tipo compromisso pelo código")
	public void tipoCompromissoNaoEncontrado() {
		CodigoNaoEncontradoException exception = Assertions.assertThrows(CodigoNaoEncontradoException.class, () -> {
			service.buscarPorId(CODIGO_INEXISTENTE);
		});
		assertThat(exception.getMessage()).isEqualTo(ConstantesApi.TIPO_NAO_ENCONTRADO);
	}

	@Test
	@DisplayName("Apaga o tipo compromisso pelo código")
	public void apagarTipoCompromisso() {
		service.apagaPorId(CODIGO);
		verify(repository).deleteById(CODIGO);
	}

	private List<TipoCompromisso> montaListaEntidade() {
		// TODO: Criar uma forma de pegar a lista de um json!
		return Arrays.asList(new TipoCompromisso(2, "Pessoais"),
			new TipoCompromisso(3, "Trabalho"));
	}

	private List<TipoCompromissoDTO> montaListaDto() {
		return Arrays.asList(new TipoCompromissoDTO(2, "Pessoais"),
			new TipoCompromissoDTO(3, "Trabalho"));
	}

	private void criaTipoCompromisso() {
		tipoCompromisso = TipoCompromisso.builder().codigo(CODIGO).descricao(DESCRICAO).build();
	}

	private void criaDTO() {
		tipoCompromissoDto = TipoCompromissoDTO.builder().codigo(CODIGO).descricao(DESCRICAO).build();
	}

	private void preparaMockBuscarPeloCodigo() {
		when(repository.findById(any())).thenReturn(Optional.of(tipoCompromisso));
		when(mapper.tipoCompromissoDTO(any(TipoCompromisso.class))).thenReturn(tipoCompromissoDto);
	}

	private void preparaMockBuscarDados() {
		when(repository.findAll()).thenReturn(montaListaEntidade());
		when(mapper.tiposDeCompromisso(montaListaEntidade())).thenReturn(montaListaDto());
	}

	private void preparaMockSalvar() {
		when(mapper.tipoCompromisso(any(TipoCompromissoDTO.class))).thenReturn(tipoCompromisso);
		when(repository.save(any(TipoCompromisso.class))).thenReturn(tipoCompromisso);
		when(mapper.tipoCompromissoDTO(any(TipoCompromisso.class))).thenReturn(tipoCompromissoDto);
	}

	private void preparaMockAtualizar() {
		when(repository.findById(any(Integer.class))).thenReturn(Optional.of(tipoCompromisso));
		when(mapper.tipoCompromisso(any(TipoCompromissoDTO.class))).thenReturn(tipoCompromisso);
		when(repository.save(any(TipoCompromisso.class))).thenReturn(tipoCompromisso);
		when(mapper.tipoCompromissoDTO(any(TipoCompromisso.class))).thenReturn(tipoCompromissoDto);
	}
}